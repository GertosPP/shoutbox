﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shoutbox.Models
{
    public class ShoutMessageModels
    {
        public int Id { get; set; }
        public String Text { get; set; }
        public String Tag { get; set; }
    }
}