﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shoutbox.Repositories
{
    public class ShoutMessagesRepository
    {
        static IDictionary<int, Models.ShoutMessageModels> messages;
        public static void Clear()
        {
            if (messages != null)
                messages.Clear();
            else
                messages = new Dictionary<int, Models.ShoutMessageModels>();
        }
        public IList<Models.ShoutMessageModels> Get(int skip, int top)
        {
            return messages.OrderByDescending(s => s.Value.Id)
                .Skip(skip).Take(top)
                .Select(s => s.Value).ToList();
        }
        public Models.ShoutMessageModels AddOrUpdate(int id, String text, String tag)
        {
            Models.ShoutMessageModels msg = null;
            if (id != 0)
                messages.TryGetValue(id, out msg);
            if (msg == null)
            {
                msg = new Models.ShoutMessageModels();
                if (messages.Keys.Count == 0)
                    msg.Id = 1;
                else
                    msg.Id = messages.Keys.Max() + 1;
            }
            msg.Tag = tag;
            msg.Text = text;
            messages[msg.Id] = msg;
            return msg;
        }
    }
}