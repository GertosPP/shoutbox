﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Shoutbox.Controllers
{
    public class ShoutMessagesController : ApiController
    {
        private Repositories.ShoutMessagesRepository _messageRepository;
        public ShoutMessagesController()
        {
            _messageRepository = new Repositories.ShoutMessagesRepository();
        }
        public IList<Models.ShoutMessageModels> GetMessages(int take = 10, int skip = 0)
        {
            return _messageRepository.Get(skip, take);
        }

        public IHttpActionResult Post([FromBody]Models.ShoutMessageModels userMessage)
        {
            Models.ShoutMessageModels newMessage =
                _messageRepository.AddOrUpdate(userMessage.Id, userMessage.Text, userMessage.Tag);

            return Ok(newMessage);
        }
    }
}
